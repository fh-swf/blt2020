[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/fh-swf%2Fblt2020/master)

# Beispiele zum Vortrag *Interaktive Übungen & Skripte mit Jupyter Notebooks*

Hier finden Sie die Beispiele zum [Blended Learning Tag 2020](https://elearning.fh-swf.de/course/view.php?id=6755):

- Beispiel-Notebook zum Thema Machine Learning
- Beispiel-Übungen mit `nbgrader`

## Live-Demo
Über das *binder* Icon oben können Sie die das Notebook direkt starten.